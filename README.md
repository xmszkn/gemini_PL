# gemini_PL.gmi 

Polska część Geminispace.

## Co to za lista?

Lista *gemini_pl.gmi* to inicjatywa mająca na celu ułatwienie dotarcia do polskich autorów w przestrzeni Gemini (ang. Geminispace). Lista jest utrzymywana przez społeczność i dostępna dla każdego, który może ją publikować w ramach swojej kapsuły Gemini. 

## Gdzie mogę zobaczyć zawartość listy?

Zawartość listy można zobaczyć otwierając plik *gemini_pl.gmi* znajdujący się w tym repozytorium tzn. pobierając repozytorium na swój dysk, lub za pomocą przeglądarki internetowej https://codeberg.org/szczezuja/gemini_PL/raw/branch/main/gemini_pl.gmi

## Co to jest Gemini i Geminispace?

Odpowiedzi dostępne w [Project Gemini FAQ](https://gemini.circumlunar.space/docs/faq.gmi)

## Kto zarządza listą?

Każdy ma prawo do modyfikacji listy *gemini_pl.gmi* poprzez zgłoszenie tzw. Pull Request do repozytorium

Instrukcja wykonania powyższego znajduje się w serwisie, gdzie zlokalizowane jest repozytorium https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/ (należy wykonać polecenie Fork, wprowadzić zmiany na swojej gałęzi (tzw. branch) i wykonać polecenie New Pull Request.

W przypadku braku możliwości wykonania powyższych kroków technicznych, możliwe jest utworzenie zgłoszenia wchodząc na adres https://codeberg.org/szczezuja/gemini_PL/issues Ręczne zgłoszenia zmian będą nanoszone w miarę dostępności. 

## Kopie lustrzane

* https://codeberg.org/szczezuja/gemini_PL
* https://gitea.citizen4.eu/mirror/gemini_PL

## Kto i jak może wykorzystać listę?

Lista *gemini_pl.gmi* dostępna jest na licencji opisanej w pliku LICENSE oraz dostępnej w pod adresem https://creativecommons.org/licenses/by-nc/4.0/deed.pl

Oznacza to, że zgodnie z licencją możesz:

* kopiować i rozpowszechniać listę *gemini_pl.gmi* w dowolnym medium i formacie

* remiksować, zmieniać i tworzyć na bazie listy *gemini_pl.gmi* inne dokumenty

* o ile jest to działanie niekomercyjne i wskażesz na źródło pochodzenia listy *gemini_pl.gmi*.

Jednym z podstawowych założeń jest, że lista *gemini_pl.gmi* będzie kopiowana na innych kapsułach polskojęzycznej części przestrzeni Gemini, tak aby popularyzowała inicjatywę.  
